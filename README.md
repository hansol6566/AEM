# 글쓴이: __Boramko__

`개인적인 어도비 프로젝트 설명을 위해 만듬 `

###1. 개발 환경 (Developer Setup)

   1.Windows 10(64bit)
   2.AEM6.0 
   3.Jdk7
   4.Bracket or sublime3
   5.Eclipse_luna
   6.Oracle_Xe
   7.Mybatis
   8.Svn
   
###2. 프로젝트 생성 및 구축 (New Project) 

   Eclipse AEM
   AEM Sample Multi-Module Project - aem-project-archetype 10
   
###3. AEM 연동 (AEM Connction)

   1. Adobe Experience Manager
   2. VaultClipse
   
###4. 컴포넌트 개발 및 방법 (Developer Components) 

   1. Create Template
   2. Create Node
   3. Create Componets (JSP)
   4. Create Componets (Sightly JavaScript OR JAVA)